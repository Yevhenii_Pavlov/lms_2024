from django.core.validators import MinLengthValidator
from django.db import models
from faker import Faker

from core.models import PersonModel
from core.utils.validators import validate_file_size
from groups.models import Group
from students.utils.validators import (
    first_name_validator,
    grade_validator,
    extension_validator,
)


class Student(PersonModel):
    first_name = models.CharField(
        max_length=120,
        validators=[MinLengthValidator(2), first_name_validator],
        verbose_name="First name",
    )
    grade = models.PositiveSmallIntegerField(default=0, validators=[grade_validator], verbose_name="Grade")
    photo = models.ImageField(
        upload_to="images/students/",
        default="images/default.jpg",
        validators=[validate_file_size],
        verbose_name="Photo",
    )
    resume = models.FileField(
        upload_to="students_resume/",
        blank=True,
        null=True,
        validators=[extension_validator, validate_file_size],
        verbose_name="Resume",
    )
    group = models.ForeignKey(
        Group,
        on_delete=models.SET_NULL,
        null=True,
        related_name="student",
        verbose_name="Group",
    )

    @classmethod
    def generate_students(cls, count: int) -> None:
        faker = Faker()
        group_ids = Group.objects.values_list("id", flat=True)
        for i in range(count):
            group_id = faker.random.choice(group_ids)
            student = Student(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                email=faker.email(),
                grade=faker.random_number(digits=2),
                birth_date=faker.date_of_birth(minimum_age=16, maximum_age=60),
                group_id=group_id,
            )
            student.save()

    def __str__(self):
        return f"{self.first_name}, {self.last_name}, {self.email}, {self.grade}, {self.birth_date}, ({self.uuid})"
