from django.core.exceptions import ValidationError


def first_name_validator(first_name: str) -> None:
    if first_name.lower() == "vova":
        raise ValidationError("Vova isn't correct name, should be Volodymyr!")


def grade_validator(grade: int) -> None:
    if grade < 0 or grade > 100:
        raise ValidationError("The grade must be between 0 and 100 points.")


def extension_validator(file_name: str) -> None:
    if file_name:
        file_extension = file_name.name.split(".")[-1].lower()
        allowed_extensions = ["pdf", "doc", "docx"]
        if file_extension not in allowed_extensions:
            raise ValidationError("Only PDF, DOC, and DOCX files are allowed.")
