from django.contrib.admin import SimpleListFilter


class GradeFilter(SimpleListFilter):
    title = "Grade"
    parameter_name = "grade"

    def lookups(self, request, model_admin):
        return [
            ("1", "0-24"),
            ("2", "25-49"),
            ("3", "50-74"),
            ("4", "75-100"),
        ]

    def queryset(self, request, queryset):
        if self.value() == "1":
            return queryset.filter(grade__gte=0, grade__lt=25)
        if self.value() == "2":
            return queryset.filter(grade__gte=25, grade__lt=50)
        if self.value() == "3":
            return queryset.filter(grade__gte=50, grade__lt=75)
        if self.value() == "4":
            return queryset.filter(grade__gte=75, grade__lte=100)
