from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.urls import reverse_lazy
from django.views.generic import CreateView, UpdateView, DeleteView, ListView

from students.forms import StudentForm
from students.models import Student


class StudentListView(LoginRequiredMixin, ListView):
    model = Student
    template_name = "students/list.html"
    context_object_name = "students"
    queryset = Student.objects.select_related("group").all()

    def get_queryset(self):
        queryset = super().get_queryset()
        search_value = self.request.GET.get("search")
        if search_value:
            or_filter = Q()
            search_fields = [
                "first_name",
                "last_name",
                "email",
                "grade",
                "birth_date",
                "group__name",
            ]
            for field in search_fields:
                or_filter |= Q(**{f"{field}__icontains": search_value})
            queryset = queryset.filter(or_filter)
        return queryset


class CreateStudentView(LoginRequiredMixin, CreateView):
    model = Student
    form_class = StudentForm
    template_name = "students/create.html"
    success_url = reverse_lazy("students:get_students")


class UpdateStudentView(LoginRequiredMixin, UpdateView):
    model = Student
    form_class = StudentForm
    template_name = "students/update.html"
    pk_url_kwarg = "uuid"
    queryset = Student.objects.all()
    success_url = reverse_lazy("students:get_students")


class DeleteStudentView(LoginRequiredMixin, DeleteView):
    model = Student
    template_name = "students/delete.html"
    pk_url_kwarg = "uuid"
    success_url = reverse_lazy("students:get_students")
