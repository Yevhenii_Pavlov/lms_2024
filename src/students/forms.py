from core.forms import PersonForm
from students.models import Student


class StudentForm(PersonForm):
    class Meta:
        model = Student
        fields = [
            "first_name",
            "last_name",
            "email",
            "grade",
            "birth_date",
            "photo",
            "resume",
            "group",
        ]
