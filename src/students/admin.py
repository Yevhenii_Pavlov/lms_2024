from django.contrib import admin
from django.urls import reverse
from django.utils.safestring import mark_safe

from students.models import Student
from students.utils.admin_filters import GradeFilter


class StudentAdminInline(admin.TabularInline):
    model = Student
    extra = 0
    show_change_link = True


@admin.register(Student)
class StudentAdmin(admin.ModelAdmin):
    list_display = (
        "first_name",
        "last_name",
        "email",
        "grade",
        "birth_date",
        "post_photo",
        "display_resume",
        "link_to_group",
    )
    readonly_fields = ("post_photo",)
    list_display_links = (
        "first_name",
        "last_name",
        "email",
    )
    list_editable = ("grade",)
    ordering = (
        "first_name",
        "last_name",
    )
    fieldsets = (
        (
            "Personal information",
            {
                "fields": (
                    "first_name",
                    "last_name",
                    "email",
                    "birth_date",
                    "photo",
                    "post_photo",
                )
            },
        ),
        (
            "Additional information",
            {
                "classes": ("collapse",),
                "fields": (
                    "grade",
                    "resume",
                    "group",
                ),
            },
        ),
    )
    date_hierarchy = "birth_date"
    list_filter = ("group__name", GradeFilter)
    save_on_top = True
    list_select_related = ("group",)
    search_fields = (
        "first_name",
        "last_name",
        "email",
    )
    list_per_page = 10

    @admin.display(description="Student photo")
    def post_photo(self, student: Student):
        return mark_safe(f"<img src='{student.photo.url}' width=50>")

    @admin.display(description="Resume")
    def display_resume(self, student: Student):
        if student.resume:
            return mark_safe(f"<a href='{student.resume.url}' target='_blank'>Available</a>")
        else:
            return "Empty"

    @admin.display(description="Group name")
    def link_to_group(self, student: Student):
        if student.group:
            return mark_safe(
                f"<a class='button' href='{reverse('admin:groups_group_change', args=[student.group.id])}'"
                f">{student.group.name}</a>"
            )
        return "No available groups"
