from django.contrib.auth import login, get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.auth.views import LoginView, LogoutView
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404  # NOQA:F401
from django.urls import reverse_lazy
from django.utils.encoding import force_str
from django.utils.http import urlsafe_base64_decode
from django.views.generic import (
    TemplateView,
    CreateView,
    RedirectView,
    UpdateView,
    DetailView,
)

from core.forms import UserRegistrationForm, UserUpdateForm
from core.services.emails import send_registration_email
from core.utils.token_generator import TokenGenerator


class Custom404View(TemplateView):
    template_name = "errors/404.html"

    def dispatch(self, request, *args, **kwargs):
        response = super().dispatch(request, *args, **kwargs)
        response.status_code = 404
        return response


class IndexView(TemplateView):
    template_name = "index.html"


class UserLoginView(LoginView):
    pass


class UserLogoutView(LogoutView):
    pass


class UserRegistrationView(CreateView):
    template_name = "registration/registration.html"
    form_class = UserRegistrationForm
    success_url = reverse_lazy("core:index")

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.is_active = False
        self.object.save()

        send_registration_email(user_instance=self.object, request=self.request)

        return super().form_valid(form)


class ActivateUserView(RedirectView):
    url = reverse_lazy("core:index")

    def get(self, request, uuid64, token, *args, **kwargs):
        try:
            pk = force_str(urlsafe_base64_decode(uuid64))
            current_user = get_user_model().objects.get(pk=pk)
        except (get_user_model().DoesNotExist, ValueError, TypeError):
            return HttpResponse("Wrong data!!!")
        if current_user and TokenGenerator().check_token(current_user, token):
            current_user.is_active = True
            current_user.save()
            login(
                request,
                current_user,
                backend="django.contrib.auth.backends.ModelBackend",
            )
            return super().get(request, *args, **kwargs)
        return HttpResponse("Wrong data!!!")


class UserProfileView(LoginRequiredMixin, DetailView):
    model = get_user_model()
    form_class = get_user_model()
    template_name = "users/user_profile.html"


class UpdateUserProfileView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = get_user_model()
    form_class = UserUpdateForm
    template_name = "users/update_user_profile.html"
    queryset = get_user_model().objects.all()

    def test_func(self):
        obj = self.get_object()
        return obj == self.request.user

    def get_success_url(self):
        return reverse_lazy("core:user_profile", kwargs={"pk": self.object.pk})
