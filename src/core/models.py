from datetime import date
from uuid import uuid4

from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.core.validators import MinLengthValidator
from django.db import models
from django.utils import timezone
from phonenumber_field.modelfields import PhoneNumberField

from core.managers import CustomerManager
from core.utils.validators import birth_date_validator, validate_file_size
from django.utils.translation import gettext_lazy as _


class UserType(models.TextChoices):
    STUDENT = "student", "Student"
    TEACHER = "teacher", "Teacher"
    MENTOR = "mentor", "Mentor"


class SexChoices(models.TextChoices):
    MALE = "male", "Male"
    FEMALE = "female", "Female"
    NO_ANSWER = "no_answer", "I don't want to answer "


class Customer(AbstractBaseUser, PermissionsMixin):
    first_name = models.CharField(_("first name"), max_length=150, blank=True)
    last_name = models.CharField(_("last name"), max_length=150, blank=True)
    email = models.EmailField(
        _("email address"),
        null=True,
        blank=True,
        error_messages={
            "unique": _("A user with that email already exists."),
        },
    )
    is_staff = models.BooleanField(
        _("staff status"),
        default=False,
        help_text=_("Designates whether the user can log into this admin site."),
    )
    is_active = models.BooleanField(
        _("active"),
        default=True,
        help_text=_(
            "Designates whether this user should be treated as active. " "Unselect this instead of deleting accounts."
        ),
    )
    date_joined = models.DateTimeField(_("date joined"), default=timezone.now)
    phone_number = PhoneNumberField(
        _("phone number"),
        null=True,
        blank=True,
        region="US",
    )
    photo = models.ImageField(
        _("photo"),
        upload_to="images/user_profile_photo/",
        default="images/default.jpg",
        validators=[validate_file_size],
    )
    location = models.CharField(_("location"), max_length=255, blank=True, null=True)
    birth_date = models.DateField(_("date of birth"), blank=True, null=True, validators=[birth_date_validator])
    user_type = models.CharField(_("user type"), blank=True, max_length=15, choices=UserType.choices)
    sex = models.CharField(_("gender"), blank=True, max_length=15, choices=SexChoices.choices)

    objects = CustomerManager()

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = _("customer")
        verbose_name_plural = _("customers")

    def clean(self):
        super().clean()
        self.email = self.__class__.objects.normalize_email(self.email)

    def get_full_name(self):
        """
        Return the first_name plus the last_name, with a space in between.
        """
        full_name = "%s %s" % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        """Return the short name for the user."""
        return self.first_name

    def get_registration_age(self):
        return f"Time on site: {timezone.now() - self.date_joined}"

    def age(self):
        today = date.today()
        return (
            today.year
            - self.birth_date.year
            - ((today.month, today.day) < (self.birth_date.month, self.birth_date.day))
        )


class PersonModel(models.Model):
    uuid = models.UUIDField(
        default=uuid4,
        primary_key=True,
        unique=True,
        editable=False,
        db_index=True,
    )
    first_name = models.CharField(
        max_length=120,
        validators=[MinLengthValidator(2)],
        verbose_name="First Name",
    )
    last_name = models.CharField(
        max_length=120,
        verbose_name="Last Name",
    )
    email = models.EmailField(
        max_length=150,
        verbose_name="Email",
    )
    birth_date = models.DateField(
        null=True,
        blank=True,
        validators=[birth_date_validator],
        verbose_name="Date of birth",
    )

    class Meta:
        abstract = True

    def age(self):
        today = date.today()
        return (
            today.year
            - self.birth_date.year
            - ((today.month, today.day) < (self.birth_date.month, self.birth_date.day))
        )
