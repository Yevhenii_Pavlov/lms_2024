from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm
from django.core.exceptions import ValidationError
from django.forms import ModelForm
from phonenumber_field.formfields import PhoneNumberField
from phonenumber_field.widgets import PhoneNumberPrefixWidget


class UserRegistrationForm(UserCreationForm):
    phone_number = PhoneNumberField(
        label="Phone Number",
        widget=PhoneNumberPrefixWidget(
            country_choices=[
                ("US", "+1"),
            ],
            attrs={"placeholder": "Format: (XXX)-XXX-XXXX"},
        ),
        help_text="Format: (XXX)-XXX-XXXX",
    )

    class Meta:
        model = get_user_model()
        fields = [
            "first_name",
            "last_name",
            "email",
            "is_staff",
            "phone_number",
            "photo",
            "location",
            "birth_date",
            "user_type",
            "sex",
            "password1",
            "password2",
        ]

    def clean(self):
        cleaned_data = super().clean()
        email = cleaned_data.get("email")
        phone_number = cleaned_data.get("phone_number")

        if not email and not phone_number:
            raise ValidationError("Insert email or phone number. At least one of them should be set.")
        elif not email:
            if get_user_model().objects.filter(phone_number=phone_number).exists():
                raise ValidationError("User with phone number already exists!!!")
        elif not phone_number:
            if get_user_model().objects.filter(email=email).exists():
                raise ValidationError("User with email already exists!!!")
        else:
            if (
                get_user_model().objects.filter(email=email).exists()
                and get_user_model().objects.filter(phone_number=phone_number).exists()
            ):
                raise ValidationError("User with these email and phone number already exists!!!")
            elif get_user_model().objects.filter(email=email).exists():
                raise ValidationError("User with this email already exists!!!")
            elif get_user_model().objects.filter(phone_number=phone_number).exists():
                raise ValidationError("User with this phone number already exists!!!")
        return cleaned_data


class PersonForm(ModelForm):
    @staticmethod
    def normalize_text(text: str) -> str:
        return text.strip().capitalize()

    def clean_first_name(self):
        if self.cleaned_data["first_name"]:
            return self.normalize_text(self.cleaned_data["first_name"])

    def clean_last_name(self):
        if self.cleaned_data["last_name"]:
            return self.normalize_text(self.cleaned_data["last_name"])

    def clean_email(self):
        email = self.cleaned_data["email"].lower()
        if "@yandex" in email:
            raise ValidationError("Yandex is forbidden to use in our country!")
        return email

    def clean(self):
        cleaned_data = super().clean()
        first_name = cleaned_data["first_name"]
        last_name = cleaned_data["last_name"]
        if first_name == last_name:
            raise ValidationError("First name and last name cannot be equal!!!")
        return cleaned_data


class UserUpdateForm(forms.ModelForm):
    phone_number = PhoneNumberField(
        label="Phone Number",
        widget=PhoneNumberPrefixWidget(
            country_choices=[
                ("US", "+1"),
            ],
            attrs={"placeholder": "Format: (XXX)-XXX-XXXX"},
        ),
        help_text="Format: (XXX)-XXX-XXXX",
    )

    class Meta:
        model = get_user_model()
        fields = [
            "first_name",
            "last_name",
            "is_staff",
            "phone_number",
            "photo",
            "location",
            "birth_date",
            "user_type",
            "sex",
        ]
