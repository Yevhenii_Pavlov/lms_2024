from datetime import date

from django.core.exceptions import ValidationError


def birth_date_validator(birth_date: date) -> None:
    if birth_date:
        today = date.today()
        age = today.year - birth_date.year - ((today.month, today.day) < (birth_date.month, birth_date.day))
        if age < 16:
            raise ValidationError("Person must be at least 16 years old.")


def validate_file_size(value):
    max_size_mb = 15
    max_size = max_size_mb * 1024 * 1024
    if value.size > max_size:
        raise ValidationError(
            f"The size of the file can't be more than {max_size_mb} MB, current size is "
            f"{round(value.size / (1024 * 1024), 2)} MB"
        )
