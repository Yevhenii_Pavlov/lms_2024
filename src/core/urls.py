from django.urls import path

from core.views import (
    IndexView,
    UserLoginView,
    UserLogoutView,
    UserRegistrationView,
    ActivateUserView,
    UpdateUserProfileView,
    UserProfileView,
)

app_name = "core"

urlpatterns = [
    path("", IndexView.as_view(), name="index"),
    path("login/", UserLoginView.as_view(), name="login"),
    path("logout/", UserLogoutView.as_view(), name="logout"),
    path("registration/", UserRegistrationView.as_view(), name="registration"),
    path(
        "activate/<str:uuid64>/<str:token>",
        ActivateUserView.as_view(),
        name="activate_user",
    ),
    path(
        "user_profile/<int:pk>",
        UserProfileView.as_view(),
        name="user_profile",
    ),
    path(
        "update_user_profile/<int:pk>",
        UpdateUserProfileView.as_view(),
        name="update_user_profile",
    ),
]
