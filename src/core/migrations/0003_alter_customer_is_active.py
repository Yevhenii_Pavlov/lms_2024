# Generated by Django 4.2.11 on 2024-04-24 19:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("core", "0002_alter_customer_is_active"),
    ]

    operations = [
        migrations.AlterField(
            model_name="customer",
            name="is_active",
            field=models.BooleanField(
                default=True,
                help_text="Designates whether this user should be treated as active. Unselect this instead of deleting accounts.",
                verbose_name="active",
            ),
        ),
    ]
