from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin
from django.utils.safestring import mark_safe


@admin.register(get_user_model())
class CustomerModel(UserAdmin):
    list_display = [
        "first_name",
        "last_name",
        "email",
        "is_staff",
        "phone_number",
        "post_photo",
        "location",
        "birth_date",
        "user_type",
        "sex",
    ]
    ordering = (
        "first_name",
        "last_name",
    )
    search_fields = (
        "first_name",
        "last_name",
        "email",
    )
    readonly_fields = ("email",)
    fieldsets = None
    date_hierarchy = "birth_date"
    list_filter = (
        "user_type",
        "sex",
    )
    list_editable = (
        "user_type",
        "sex",
    )
    save_on_top = True
    list_per_page = 10

    @admin.display(description="User photo")
    def post_photo(self, user: get_user_model()):
        return mark_safe(f"<img src='{user.photo.url}' width=50>")
