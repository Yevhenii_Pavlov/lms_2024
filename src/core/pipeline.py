def cleanup_social_account(backend, user=None, *args, **kwargs):
    if backend.name == "google-oauth2" and kwargs.get("response", {}).get("picture"):
        user.photo = kwargs["response"]["picture"]
        user.save()
    return {
        "user": user,
    }
