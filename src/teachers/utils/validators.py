from django.core.exceptions import ValidationError


def rating_validator(rating: float | int) -> None:
    if rating < 0 or rating > 10:
        raise ValidationError("The rating can't be less than 0 and more than 10")


def phone_number_validator(phone_number: str) -> None:

    for char in phone_number[1:]:
        if not char.isdigit():
            raise ValidationError("The phone number must contain only digits after '+'")

    if len(phone_number) > 15 or phone_number[0] != "+":
        raise ValidationError("The phone number must begin with '+' and must not contain more than 15 characters.")
