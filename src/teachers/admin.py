from django.contrib import admin
from django.urls import reverse
from django.utils.html import format_html
from django.utils.safestring import mark_safe

from teachers.models import Teacher


class TeacherAdminInline(admin.TabularInline):
    model = Teacher.group.through
    extra = 0
    show_change_link = 0
    can_delete = False


@admin.register(Teacher)
class TeacherAdmin(admin.ModelAdmin):
    list_display = (
        "first_name",
        "last_name",
        "email",
        "birth_date",
        "post_photo",
        "degree",
        "rating",
        "phone_number",
        "group_count",
        "link_to_group",
    )
    fieldsets = (
        (
            "Personal information",
            {
                "fields": (
                    "first_name",
                    "last_name",
                    "email",
                    "birth_date",
                    "phone_number",
                    "photo",
                    "post_photo",
                )
            },
        ),
        (
            "Additional information",
            {
                "classes": ("collapse",),
                "fields": (
                    "degree",
                    "rating",
                    "group",
                ),
            },
        ),
    )
    list_display_links = (
        "first_name",
        "last_name",
        "email",
    )
    search_fields = (
        "first_name",
        "last_name",
        "email",
    )
    readonly_fields = ("post_photo",)
    ordering = (
        "first_name",
        "last_name",
    )
    date_hierarchy = "birth_date"
    save_on_top = True
    list_per_page = 10
    filter_horizontal = ("group",)

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        return queryset.prefetch_related("group")

    @admin.display(description="Photo")
    def post_photo(self, teacher: Teacher):
        return mark_safe(f"<img src='{teacher.photo.url}' width=50>")

    @admin.display(description="Number of groups")
    def group_count(self, teacher: Teacher):
        if teacher.group:
            return teacher.group.count()
        return "No groups"

    @admin.display(description="Group names")
    def link_to_group(self, teacher: Teacher):
        if teacher.group:
            groups = teacher.group.all()
            links = []

            for group in groups:
                links.append(
                    f"<a class='button' href='{reverse('admin:groups_group_change', args=[group.id])}'"
                    f">{group.name}</a>"
                )
            return format_html("</br></br>".join(links))
        return "No available groups"
