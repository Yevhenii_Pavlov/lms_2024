from core.forms import PersonForm
from teachers.models import Teacher


class TeacherForm(PersonForm):
    class Meta:
        model = Teacher
        fields = [
            "first_name",
            "last_name",
            "email",
            "birth_date",
            "degree",
            "rating",
            "phone_number",
            "photo",
            "group",
        ]
