from django.db import models
from faker import Faker

from core.models import PersonModel
from core.utils.validators import validate_file_size
from groups.models import Group
from teachers.utils.validators import (
    rating_validator,
    phone_number_validator,
)


class Teacher(PersonModel):
    degree = models.CharField(max_length=200, blank=True, null=True)
    rating = models.DecimalField(max_digits=3, decimal_places=1, default=0.0, validators=[rating_validator])
    phone_number = models.CharField(max_length=15, blank=True, null=True, validators=[phone_number_validator])
    photo = models.ImageField(
        upload_to="images/teachers/", default="images/default.jpg", validators=[validate_file_size]
    )
    group = models.ManyToManyField(Group, related_name="teacher")

    @classmethod
    def generate_teachers(cls, count: int) -> None:
        faker = Faker()
        for i in range(count):
            teacher = Teacher(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                email=faker.email(),
                birth_date=faker.date_of_birth(),
                degree=faker.word(),
                rating=faker.pyfloat(left_digits=1, right_digits=1, positive=True),
                phone_number=faker.phone_number(),
            )
            teacher.save()

    def __str__(self):
        return (
            f"{self.first_name}, {self.last_name}, {self.email}, {self.rating}, {self.birth_date}, "
            f"{self.phone_number}, ({self.uuid})"
        )
