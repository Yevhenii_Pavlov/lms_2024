# Generated by Django 4.2.11 on 2024-05-01 15:18

import core.utils.validators
import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("teachers", "0001_initial"),
    ]

    operations = [
        migrations.AlterField(
            model_name="teacher",
            name="birth_date",
            field=models.DateField(
                blank=True,
                null=True,
                validators=[core.utils.validators.birth_date_validator],
                verbose_name="Date of birth",
            ),
        ),
        migrations.AlterField(
            model_name="teacher",
            name="email",
            field=models.EmailField(max_length=150, verbose_name="Email"),
        ),
        migrations.AlterField(
            model_name="teacher",
            name="first_name",
            field=models.CharField(
                max_length=120,
                validators=[django.core.validators.MinLengthValidator(2)],
                verbose_name="First Name",
            ),
        ),
        migrations.AlterField(
            model_name="teacher",
            name="last_name",
            field=models.CharField(max_length=120, verbose_name="Last Name"),
        ),
    ]
