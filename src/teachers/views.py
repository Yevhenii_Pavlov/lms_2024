from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q

from django.urls import reverse_lazy
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    DeleteView,
    UpdateView,
)


from teachers.forms import TeacherForm
from teachers.models import Teacher


class TeacherListView(LoginRequiredMixin, ListView):
    model = Teacher
    template_name = "teachers/list.html"
    context_object_name = "teachers"
    queryset = Teacher.objects.prefetch_related("group").all()

    def get_queryset(self):
        queryset = super().get_queryset()
        search_value = self.request.GET.get("search")
        if search_value:
            or_filter = Q()
            search_fields = [
                "first_name",
                "last_name",
                "email",
                "degree",
                "rating",
                "phone_number",
                "birth_date",
                "group__name",
            ]
            for field in search_fields:
                or_filter |= Q(**{f"{field}__icontains": search_value})
            queryset = queryset.filter(or_filter)
        return queryset


class TeacherDetailView(LoginRequiredMixin, DetailView):
    model = Teacher
    template_name = "teachers/teacher_details.html"
    pk_url_kwarg = "uuid"
    context_object_name = "teacher"


class CreateTeacherView(LoginRequiredMixin, CreateView):
    model = Teacher
    template_name = "teachers/create.html"
    form_class = TeacherForm
    success_url = reverse_lazy("teachers:get_teachers")


class UpdateTeacherView(LoginRequiredMixin, UpdateView):
    model = Teacher
    form_class = TeacherForm
    template_name = "teachers/update.html"
    pk_url_kwarg = "uuid"
    queryset = Teacher.objects.all()
    success_url = reverse_lazy("teachers:get_teachers")


class DeleteTeacherView(LoginRequiredMixin, DeleteView):
    model = Teacher
    template_name = "teachers/delete.html"
    pk_url_kwarg = "uuid"
    success_url = reverse_lazy("teachers:get_teachers")
