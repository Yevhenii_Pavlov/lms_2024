from django.urls import path

from teachers.views import (
    TeacherDetailView,
    TeacherListView,
    CreateTeacherView,
    UpdateTeacherView,
    DeleteTeacherView,
)

app_name = "teachers"

urlpatterns = [
    path("", TeacherListView.as_view(), name="get_teachers"),
    path("create/", CreateTeacherView.as_view(), name="create_teacher"),
    path("update/<uuid:uuid>", UpdateTeacherView.as_view(), name="update_teacher"),
    path("delete/<uuid:uuid>", DeleteTeacherView.as_view(), name="delete_teacher"),
    path(
        "teacher_detail/<uuid:uuid>",
        TeacherDetailView.as_view(),
        name="get_teacher_details",
    ),
]
