from datetime import datetime, timedelta

from django.db import models
from faker import Faker

from groups.utils.validator import (
    group_rating_validator,
    number_of_lessons_validator,
    expected_end_date_validator,
)


class Group(models.Model):
    name = models.CharField(max_length=255)
    creation_date = models.DateField(auto_now_add=True)
    rating = models.DecimalField(max_digits=3, decimal_places=1, default=0.0, validators=[group_rating_validator])
    description = models.TextField(blank=True, null=True)
    number_of_lessons = models.IntegerField(default=0, validators=[number_of_lessons_validator])
    expected_end_date = models.DateField(null=True, blank=True, validators=[expected_end_date_validator])

    @classmethod
    def generate_groups(cls, count: int) -> None:
        faker = Faker()
        for i in range(count):
            today = datetime.now().date()
            end_date_max = today + timedelta(days=365)
            group = Group(
                name=faker.word(),
                rating=faker.pyfloat(left_digits=1, right_digits=1, positive=True),
                description=faker.paragraph(nb_sentences=3),
                number_of_lessons=faker.random_number(digits=2),
                expected_end_date=faker.date_between(start_date=today, end_date=end_date_max),
            )
            group.save()

    def __str__(self):
        return f"{self.name}, {self.id}"
