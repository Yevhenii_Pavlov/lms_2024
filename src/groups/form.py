from django import forms
from django.forms import ModelForm
from groups.models import Group


class GroupForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(GroupForm, self).__init__(*args, **kwargs)
        instance = kwargs.get("instance")
        if instance:
            self.fields["creation_date"] = forms.DateField(
                label="Creation Date", disabled=True, initial=instance.creation_date
            )

    class Meta:
        model = Group
        fields = [
            "name",
            "rating",
            "description",
            "number_of_lessons",
            "expected_end_date",
        ]

    def clean_group_name(self):
        return self.cleaned_data["name"].strip().capitalize()
