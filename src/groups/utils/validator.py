from django.core.exceptions import ValidationError
from django.utils import timezone


def group_rating_validator(rating: int | float) -> None:
    if rating < 0 or rating > 10:
        raise ValidationError("The rating can't be less than 0 and more than 10")


def number_of_lessons_validator(number_of_lesson: int) -> None:
    if number_of_lesson < 0 or number_of_lesson > 100:
        raise ValidationError("The number of lessons can't be less than 0 and more than 100")


def expected_end_date_validator(end_date: timezone) -> None:
    today = timezone.now().date()
    if end_date < today:
        raise ValidationError("The end date cannot be less than today.")
