# Generated by Django 4.2.11 on 2024-04-24 19:00

from django.db import migrations, models
import groups.utils.validator


class Migration(migrations.Migration):

    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name="Group",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("name", models.CharField(max_length=255)),
                ("creation_date", models.DateField(auto_now_add=True)),
                (
                    "rating",
                    models.DecimalField(
                        decimal_places=1,
                        default=0.0,
                        max_digits=3,
                        validators=[groups.utils.validator.group_rating_validator],
                    ),
                ),
                ("description", models.TextField(blank=True, null=True)),
                (
                    "number_of_lessons",
                    models.IntegerField(
                        default=0,
                        validators=[groups.utils.validator.number_of_lessons_validator],
                    ),
                ),
                (
                    "expected_end_date",
                    models.DateField(
                        blank=True,
                        null=True,
                        validators=[groups.utils.validator.expected_end_date_validator],
                    ),
                ),
            ],
        ),
    ]
