from django.contrib import admin
from django.db.models import Count

from groups.models import Group
from students.admin import StudentAdminInline
from teachers.admin import TeacherAdminInline


@admin.register(Group)
class GroupAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "description",
        "number_of_lessons",
        "expected_end_date",
        "rating",
        "creation_date",
        "count_student",
    )
    fieldsets = (
        (
            "Main information",
            {
                "fields": (
                    "name",
                    "description",
                    "expected_end_date",
                )
            },
        ),
        (
            "Additional information",
            {
                "classes": ("collapse",),
                "fields": (
                    "number_of_lessons",
                    "rating",
                    "creation_date",
                ),
            },
        ),
    )
    inlines = (
        StudentAdminInline,
        TeacherAdminInline,
    )
    search_fields = (
        "name",
        "description",
        "email",
    )
    ordering = ("rating",)
    readonly_fields = ("creation_date",)
    date_hierarchy = "expected_end_date"
    list_per_page = 10
    save_on_top = True
    actions = ("count_to_zero_lessons",)

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        return queryset.annotate(number_of_students=Count("student"))

    @admin.display(description="Number of student")
    def count_student(self, group: Group):
        if group.student:
            return group.number_of_students
        return 0

    @admin.action(description="Set to zero number of lessons")
    def count_to_zero_lessons(self, request, queryset):
        queryset.update(number_of_lessons=0)
