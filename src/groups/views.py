from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q, Count
from django.urls import reverse_lazy
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView,
)

from groups.form import GroupForm
from groups.models import Group


class GroupListView(LoginRequiredMixin, ListView):
    model = Group
    template_name = "groups/list.html"
    context_object_name = "groups"
    queryset = Group.objects.prefetch_related("teacher").annotate(student_count=Count("student"))

    def get_queryset(self):
        queryset = super().get_queryset()
        search_value = self.request.GET.get("search")
        if search_value:
            or_filter = Q()
            search_field = [
                "name",
                "rating",
                "description",
                "number_of_lessons",
                "expected_end_date",
                "teacher__first_name",
                "teacher__last_name",
            ]
            for field in search_field:
                or_filter |= Q(**{f"{field}__icontains": search_value})
            queryset = queryset.filter(or_filter)
        return queryset


class GroupDetailView(LoginRequiredMixin, DetailView):
    model = Group
    template_name = "groups/group_details.html"
    queryset = Group.objects.all()
    context_object_name = "group"


class CreateGroupView(LoginRequiredMixin, CreateView):
    model = Group
    form_class = GroupForm
    template_name = "groups/create.html"
    success_url = reverse_lazy("groups:get_groups")


class UpdateGroupView(LoginRequiredMixin, UpdateView):
    model = Group
    form_class = GroupForm
    queryset = Group.objects.all()
    template_name = "groups/update.html"
    success_url = reverse_lazy("groups:get_groups")


class DeleteGroupView(LoginRequiredMixin, DeleteView):
    model = Group
    template_name = "groups/delete.html"
    success_url = reverse_lazy("groups:get_groups")
